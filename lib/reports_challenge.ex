defmodule ReportsChallenge do
  alias ReportsChallenge.Parser

  @months %{
    "1" => "janeiro",
    "2" => "fevereiro",
    "3" => "março",
    "4" => "abril",
    "5" => "maio",
    "6" => "junho",
    "7" => "julho",
    "8" => "agosto",
    "9" => "setembro",
    "10" => "outubro",
    "11" => "novembro",
    "12" => "dezembro"
  }

  @years [
    "2016",
    "2017",
    "2018",
    "2019",
    "2020"
  ]

  @names [
    "Cleiton",
    "Daniele",
    "Danilo",
    "Diego",
    "Giuliano",
    "Jakeliny",
    "Joseph",
    "Mayk",
    "Rafael",
    "Vinicius"
  ]

  def build(filename) do
    filename
    |> Parser.parse_file()
    |> Enum.reduce(report_acc(), fn line, report ->
      sum_values(line, report)
      # report
    end)
  end

  defp sum_values([name, hour, _day, month, _year], %{
         "all_hours" => all_hours,
         "hours_per_month" => hours_per_month,
         "hours_per_year" => hours_per_year
       }) do
    all_hours = Map.put(all_hours, name, all_hours[name] + hour)

    # hours_per_month =
    #   Map.put(
    #     hours_per_month,
    #     name
    #     10
    #   )

    # @months[to_string(month)]
    build_report(all_hours, hours_per_month, hours_per_year)
  end

  defp report_acc do
    %{
      "all_hours" => Enum.into(@names, %{}, &{&1, 0}),
      "hours_per_month" => Enum.into(@names, %{}, &{&1, interate_over_months()}),
      "hours_per_year" => Enum.into(@names, %{}, &{&1, interate_over_years()})
    }
  end

  defp build_report(all_hours, hours_per_month, hours_per_year),
    do: %{
      "all_hours" => all_hours,
      "hours_per_month" => hours_per_month,
      "hours_per_year" => hours_per_year
    }

  defp interate_over_months() do
    Map.new(@months, fn {_k, v} ->
      {v, 0}
    end)
  end

  defp interate_over_years() do
    Map.new(@years, fn year ->
      {year, 0}
    end)
  end
end
